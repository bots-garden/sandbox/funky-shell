# Funky shell function

## publish the function

```shell
curl --header "ADMIN_TOKEN: ILOVEPANDA" \
  -F "function=hello" \
  -F "branch=master" \
  -F "file=@./config.yml" \
  -F "file=@./hello.sh" \
  http://funky.test/publish
```

## invoke a function

```shell
curl http://funky.test/function/hello/master/"Bob Morane"
```

## remove a function

```shell
curl --header "ADMIN_TOKEN: ILOVEPANDA" -X "DELETE" http://funky.test/function/hello/master
```
